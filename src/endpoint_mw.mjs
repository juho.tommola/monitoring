import logger from './logger.mjs';

const endpoint_mw = (req, res, next) => {
    logger.http(`[ENDPOINT] ${req.method} '${req.path}'`);
    next();
};

export default endpoint_mw;