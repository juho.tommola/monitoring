export default class Counter {
    constructor() {
        this.count = 0;
    }

    increase() {
        this.count += 1;
        return this.count;
    }

    read() {
        return this.count;
    }

    zero() {
        this.count = 0;
        return this.count;
    }
}