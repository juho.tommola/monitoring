import express from 'express';
import router from './router.mjs';
import endpoint_mw from './endpoint_mw.mjs';

const app = express();
const PORT = 3000;

app.use(endpoint_mw);
app.use(router);

app.listen(PORT, () => {
  console.log(`Server running at http://localhost:${PORT}/`);
});