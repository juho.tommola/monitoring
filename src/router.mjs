import express from 'express';
import Counter from './counter.mjs';

const router = express.Router();
const counter = new Counter();

router.get('/counter-increase', (req, res) => {
    res.status(200).header('Content-Type', 'text/html; charset=utf-8').send(`${counter.increase()}`);
});

router.get('/counter-read', (req, res) => {
    res.status(200).header('Content-Type', 'text/html; charset=utf-8').send(`${counter.read()}`);
});

router.get('/counter-zero', (req, res) => {
    res.status(200).header('Content-Type', 'text/html; charset=utf-8').send(`${counter.zero()}`);
});

router.all('*', (req, res) => {
    res.status(404).contentType('text/html; charset=utf-8').send("Resource not found.");
  });

export default router;